---
title: Blog
visible: true
git_repo_edit_url: 'https://gitlab.com/hibbitts-design/grav-gantry5/tree/master/pages/03.blog'
content:
    items: '@self.children'
    leading: 0
    columns: 2
    limit: 5
    order:
        by: date
        dir: desc
    show_date: false
    pagination: true
    url_taxonomy_filters: true
---

